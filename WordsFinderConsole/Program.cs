﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WordsFinder;

namespace WordsFinderConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Min length: ");
            uint minLength = uint.Parse(Console.ReadLine());
            var wf = new WordsFinder.WordsFinder("C:\\Users\\donil\\Projects\\MultithreadingTest", minLength, countThreads:10);
            IEnumerable<ItemCount<string>> result = wf.Run();
            foreach (ItemCount<string> w in result)
            {
                Console.WriteLine(string.Format("{0}: {1}", w.Value, w.Count));
            }
            Console.ReadKey();
        }
    }
}
