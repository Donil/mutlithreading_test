﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsFinder
{
    public class ItemCount<T>
    {
        private T _value;

        public ItemCount(T value)
        {
            Count = 1;
            _value = value;
        }

        public T Value
        {
            get { return _value; }
        }

        public ulong Count { get; set; }
    }
}
