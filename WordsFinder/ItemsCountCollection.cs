﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsFinder
{
    public class ItemsCountCollection<T> : IEnumerable<ItemCount<T>>
    {
        private List<ItemCount<T>> _items;
        private readonly object _itemsLocker;

        public ItemsCountCollection()
        {
            _items = new List<ItemCount<T>>();
            _itemsLocker = new object();
        }

        public void Add(T value)
        {
            lock (_itemsLocker)
            {
                ItemCount<T> item = _items.FirstOrDefault(x => Equals(x.Value, value));
                if (item == null)
                {
                    item = new ItemCount<T>(value);
                    _items.Add(item);
                    return;
                }
                item.Count++;
            }
        }

        public IEnumerator<ItemCount<T>> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public override string ToString()
        {
            return string.Format("Count = {0}", _items.Count);
        }
    }
}
