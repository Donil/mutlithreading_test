﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Text.RegularExpressions;

namespace WordsFinder
{
    public class WordsFinder
    {
        private readonly string _directory;
        private readonly bool _recursive;
        private readonly ConcurrentQueue<FileInfo> _files;
        private uint _countThreads;
        private ItemsCountCollection<string> _words;
        private readonly uint _minWordLength;
        //
        private Thread _getFilesThread;
        private List<Thread> _workThreads;
        private CancellationTokenSource _cancelTokenSource;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory">Директория для поиска файлов</param>
        /// <param name="recursive">Использовать рекурсивный поиск</param>
        public WordsFinder(string directory, uint minWordLength, uint countThreads = 1, bool recursive = true)
        {
            if (string.IsNullOrEmpty(directory))
                throw new ArgumentNullException("Directory path can not be null");
            if (minWordLength == 0)
                throw new ArgumentException("Min length can not be zero");
            if (countThreads == 0)
                throw new ArgumentException("Count threads can not be zero");
            _directory = directory;
            _minWordLength = minWordLength;
            _recursive = recursive;
            _files = new ConcurrentQueue<FileInfo>();
            _countThreads = countThreads;
            _words = new ItemsCountCollection<string>();
        }

        public IEnumerable<ItemCount<string>> Run()
        {
            _cancelTokenSource = new CancellationTokenSource();
            _workThreads = new List<Thread>();
            _getFilesThread = new Thread(() => GetFilesForProcess(_cancelTokenSource.Token));
            _workThreads.Add(_getFilesThread);
            _getFilesThread.Start();
            for (int i = 0; i < _countThreads; i++)
            {
                var thread = new Thread(() => FindWorksInFile(_cancelTokenSource.Token));
                _workThreads.Add(thread);
                thread.Start();
            }
            foreach (Thread t in _workThreads)
            {
                t.Join();
            }
            return _words.OrderByDescending(x => x.Count).Take(10);
        }

        public void Stop()
        {
            if (_cancelTokenSource == null)
                return;
            _cancelTokenSource.Cancel();
        }

        private void GetFilesForProcess(CancellationToken token)
        {
            try
            {
                var di = new DirectoryInfo(_directory);
                IEnumerable<FileInfo> filesInfo = di.GetFiles("*.cs", _recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
                foreach (FileInfo fileInfo in filesInfo)
                {
                    token.ThrowIfCancellationRequested();
                    _files.Enqueue(fileInfo);
                }
                // Чтобы сообщить потокам, что больше нечего обрабатывать, добавляем пустые элементы
                for (int i = 0; i < _countThreads; i++)
                {
                    _files.Enqueue(null);
                }
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Stop thread find files");
            }
            Console.WriteLine("Find files completed");
        }

        private void FindWorksInFile(CancellationToken token)
        {
            FileInfo fi;
            try
            {
                do
                {
                    token.ThrowIfCancellationRequested();
                    while (!_files.TryDequeue(out fi))
                    {
                        token.ThrowIfCancellationRequested();
                        // Если в очереди ничего нет, то ждем
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                    }
                    if (fi == null)
                        return; // Если пришел null, значит работы больше нет
                    using (StreamReader sr = fi.OpenText())
                    {
                        var result = new List<string>();
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            // Выбираем только "слова" с длиной от 10 симовлов
                            MatchCollection matches = Regex.Matches(line, string.Format(@"\w{{{0},}}", _minWordLength));
                            foreach (Match m in matches)
                            {
                                _words.Add(m.Value);
                            }
                        }
                    }
                    Console.WriteLine(string.Format("Find in file {0} complete", fi.FullName));
                }
                while (fi != null);
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Stop thread process file");
            }
        }

        public event Action<IEnumerable<ItemCount<string>>> Completed;
    }
}
